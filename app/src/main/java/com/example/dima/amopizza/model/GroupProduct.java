package com.example.dima.amopizza.model;


import android.os.Parcel;
import android.os.Parcelable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;


@AllArgsConstructor
@Getter
@Setter
public class GroupProduct implements Parcelable {
	String name;
	ArrayList<Product> products;

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeList(this.products);
	}

	protected GroupProduct(Parcel in) {
		this.name = in.readString();
		this.products = new ArrayList<Product>();
		in.readList(this.products, Product.class.getClassLoader());
	}

	public static final Creator<GroupProduct> CREATOR = new Creator<GroupProduct>() {
		@Override
		public GroupProduct createFromParcel(Parcel source) {
			return new GroupProduct(source);
		}

		@Override
		public GroupProduct[] newArray(int size) {
			return new GroupProduct[size];
		}
	};
}
