package com.example.dima.amopizza.controller;

import android.content.Intent;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import com.example.dima.amopizza.R;
import com.example.dima.amopizza.activity.DetailActivity;
import com.example.dima.amopizza.activity.GridActivity;
import com.example.dima.amopizza.model.Product;

import java.util.ArrayList;

public class RVAdapter extends RecyclerView.Adapter<ProductViewHolder> {

	private ArrayList<Product> products;
	GridActivity parent;
	int optSize;

	public RVAdapter(GridActivity parent, ArrayList<Product> products) {
		this.products = products;
		this.parent = parent;
		Display display = parent.getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int w = size.x;
		int h = size.y;
		if (w < h) {
			optSize = w;
		} else {
			optSize = h;
		}


	}

	@Override
	public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_card, parent, false);
		ProductViewHolder pvh = new ProductViewHolder(v);
		return pvh;
	}

	@Override
	public void onBindViewHolder(ProductViewHolder holder, int position) {
		final Product currentProd = products.get(position);
		holder.productNameCard.setText(currentProd.getName());
		holder.productDescriptionCard.setText(currentProd.getDescription());
		holder.sizePriceCard.setText("Цена " + currentProd.getSize_price().get(0));
		Glide.with(parent)
				.load(currentProd.getImageUrl())
				.placeholder(R.drawable.image_holder)
				.override(optSize, optSize)
				.fitCenter()
				.into(holder.productImage);
		holder.cv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(parent, DetailActivity.class);
				intent.putExtra("product", currentProd);
				parent.startActivity(intent);
			}
		});

	}

	@Override
	public int getItemCount() {
		return products.size();
	}
}
