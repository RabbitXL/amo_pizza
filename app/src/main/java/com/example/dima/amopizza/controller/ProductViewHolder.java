package com.example.dima.amopizza.controller;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.dima.amopizza.R;

public class ProductViewHolder extends RecyclerView.ViewHolder {
	ImageView productImage;
	TextView sizePriceCard;
	TextView productDescriptionCard;
	TextView productNameCard;
	CardView cv;

	public ProductViewHolder(View itemView) {
		super(itemView);
		cv = itemView.findViewById(R.id.cv);
		productNameCard = itemView.findViewById(R.id.productNameCard);
		productDescriptionCard = itemView.findViewById(R.id.descriptionProductCard);
		sizePriceCard = itemView.findViewById(R.id.aviableSizeCard);
		productImage = itemView.findViewById(R.id.productImageCard);

	}
}
