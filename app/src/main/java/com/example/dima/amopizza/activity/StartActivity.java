package com.example.dima.amopizza.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.LinearLayout;
import com.example.dima.amopizza.R;
import com.example.dima.amopizza.controller.MyMenuListener;
import com.example.dima.amopizza.model.GroupProduct;
import lombok.Getter;

import java.util.ArrayList;

public class StartActivity extends AppCompatActivity {


	@Getter
	ArrayList<GroupProduct> catalog;
	@Getter
	final int userId = 6000;
	private int countId = 0;
	private MyMenuListener menuListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);

		//catalog = new ArrayList<GroupProduct>();
		Intent intent = getIntent();
		catalog = intent.getParcelableArrayListExtra("catalog");

		makeButtonCategory(catalog);

	}

	public void makeButtonCategory(ArrayList<GroupProduct> catalog) {
		LinearLayout menuLayout = (LinearLayout) findViewById(R.id.menuLayout);
		for (GroupProduct gp : catalog
				) {

			Button b = new Button(getApplicationContext());
			b.setText(gp.getName());
			LinearLayout.LayoutParams lParam = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT
			);

			b.setLayoutParams(lParam);
			b.setId(userId + countId);
			menuListener = new MyMenuListener(this);
			b.setOnClickListener(menuListener);
			menuLayout.addView(b);
			countId++;
		}
	}


}
