package com.example.dima.amopizza.model;

import android.os.Parcel;
import android.os.Parcelable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@ToString
@Getter
@Setter
public class Product implements Parcelable {
	Integer id;
	String name;
	String imageUrl;
	String description;
	List<SizePrice> size_price;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.name);
		dest.writeString(this.imageUrl);
		dest.writeString(this.description);
		dest.writeList(this.size_price);
	}

	protected Product(Parcel in) {
		this.id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.name = in.readString();
		this.imageUrl = in.readString();
		this.description = in.readString();
		this.size_price = new ArrayList<SizePrice>();
		in.readList(this.size_price, SizePrice.class.getClassLoader());
	}

	public static final Creator<Product> CREATOR = new Creator<Product>() {
		@Override
		public Product createFromParcel(Parcel source) {
			return new Product(source);
		}

		@Override
		public Product[] newArray(int size) {
			return new Product[size];
		}
	};
}
