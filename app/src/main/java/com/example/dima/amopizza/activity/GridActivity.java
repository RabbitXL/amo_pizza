package com.example.dima.amopizza.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.example.dima.amopizza.R;
import com.example.dima.amopizza.controller.RVAdapter;
import com.example.dima.amopizza.model.GroupProduct;
import com.example.dima.amopizza.model.Product;

import java.util.ArrayList;

public class GridActivity extends AppCompatActivity {

	private ArrayList<GroupProduct> catalog;
	ArrayList<Product> products;
	int groupNumber;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_grid);
		Intent intent = getIntent();
		catalog = intent.getParcelableArrayListExtra("catalog");
		groupNumber = intent.getIntExtra("groupNumber", groupNumber);
		products = catalog.get(groupNumber).getProducts();
		RVAdapter adapter = new RVAdapter(this, products);
		RecyclerView rv = (RecyclerView) findViewById(R.id.recyclView);
		rv.setHasFixedSize(true);
		LinearLayoutManager lineLayoutManager = new LinearLayoutManager(this);
		rv.setLayoutManager(lineLayoutManager);
		rv.setAdapter(adapter);


	}
}
