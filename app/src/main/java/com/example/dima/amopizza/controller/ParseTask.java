package com.example.dima.amopizza.controller;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.example.dima.amopizza.activity.SplashActivity;
import com.example.dima.amopizza.activity.StartActivity;
import com.example.dima.amopizza.model.GroupProduct;
import com.example.dima.amopizza.model.Product;
import com.example.dima.amopizza.model.SizePrice;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ParseTask extends AsyncTask<Void, Void, String> {

	private static final String TAG = "info";

	SplashActivity parent;

	private ArrayList<GroupProduct> catalog;

	public ParseTask(SplashActivity parent) {
		this.parent = parent;

	}

	protected void onPreExecute() {

		Toast toast = Toast.makeText(
				parent.getApplicationContext(),
				"Загрузка каталога, пожалуйста подождите", Toast.LENGTH_SHORT);
		toast.show();
	}

	protected void onPostExecute(String response) {

		// response = response.substring(1,response.length());
		// responseView.setText(response);
		try {


			//responseView.setText(i.toString());
			// responseView.setText(response);
			catalog = parseBase(response);
			Intent intent = new Intent(parent, StartActivity.class);
			intent.putParcelableArrayListExtra("catalog", catalog);
			parent.startActivity(intent);
			parent.finish();


		} catch (JSONException e) {
			e.printStackTrace();

		}


	}

	private ArrayList<GroupProduct> parseBase(String response) throws JSONException {
		ArrayList<GroupProduct> arGrPr = new ArrayList<GroupProduct>();

		JSONArray jAr = new JSONArray(response);
		for (int i = 0; i < jAr.length(); i++) {
			JSONObject jObjGr = jAr.getJSONObject(i);
			String grName = jObjGr.getString("gr_name");
			JSONArray jObjPr = jObjGr.getJSONArray("products");
			ArrayList<Product> prAr = new ArrayList<Product>();
			for (int j = 0; j < jObjPr.length(); j++) {
				JSONObject jsonOneProduct = jObjPr.getJSONObject(j);
				Integer id = jsonOneProduct.getInt("id");
				String nameProduct = jsonOneProduct.getString("name");
				String imgUrl = jsonOneProduct.getString("img_url");
				String desc = jsonOneProduct.getString("desc");
				JSONArray jArSizePrice = jsonOneProduct.getJSONArray("size_price");
				ArrayList<SizePrice> sizePrice = new ArrayList<SizePrice>();
				for (int k = 0; k < jArSizePrice.length(); k++) {
					JSONObject jSizePrice = jArSizePrice.getJSONObject(k);
					String size = jSizePrice.getString("size");
					String price = jSizePrice.getString("price");
					sizePrice.add(new SizePrice(size, price));
				}
				Product product = new Product(id, nameProduct, imgUrl, desc, sizePrice);
				prAr.add(product);
			}
			GroupProduct groupProduct = new GroupProduct(grName, prAr);
			arGrPr.add(groupProduct);
		}

		return arGrPr;
	}

	@Override
	protected String doInBackground(Void... voids) {
		String url = "http://amopizza.ru/goods2.json";
		String jsonString = "";
		try {

			jsonString = getUrlString(url);
			Log.i(TAG, "Fetched contents of URL: " + jsonString);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonString;
	}

	private String getUrlString(String url) throws IOException {
		return new String(getUrlBytes(url));
	}

	private byte[] getUrlBytes(String urlSpec) throws IOException {
		URL url = new URL(urlSpec);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			InputStream in = connection.getInputStream();
			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new IOException(connection.getResponseMessage() +
						": with " +
						urlSpec);
			}
			int bytesRead = 0;
			byte[] buffer = new byte[1024];
			while ((bytesRead = in.read(buffer)) > 0) {
				out.write(buffer, 0, bytesRead);
			}
			out.close();
			return out.toByteArray();
		} finally {
			connection.disconnect();
		}
	}


}






