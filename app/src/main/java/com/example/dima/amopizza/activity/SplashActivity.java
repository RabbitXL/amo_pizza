package com.example.dima.amopizza.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.example.dima.amopizza.controller.ParseTask;

public class SplashActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ParseTask pt = new ParseTask(this);
		pt.execute();
	}

}
