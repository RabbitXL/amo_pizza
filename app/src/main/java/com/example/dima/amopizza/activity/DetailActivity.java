package com.example.dima.amopizza.activity;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.dima.amopizza.R;
import com.example.dima.amopizza.model.Product;
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

public class DetailActivity extends AppCompatActivity implements DiscreteSeekBar.OnProgressChangeListener {

	private TextView prodName;
	private ImageView prodImage;
	private TextView prodDesc;
	private DiscreteSeekBar seekBar;
	private Product product;
	private TextView sizePrice;
	private int optSize;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);

		int w = size.x;
		int h = size.y;
		if (w < h) {
			optSize = w;
		} else {
			optSize = h;
		}
		Intent intent = getIntent();

		product = intent.getParcelableExtra("product");
		prodName = (TextView) findViewById(R.id.productName);
		prodName.setText(product.getName());
		prodImage = (ImageView) findViewById(R.id.productImage);
		Glide.with(this)
				.load(product.getImageUrl())
				.placeholder(R.drawable.image_holder)
				.override(optSize, optSize)
				.fitCenter()
				.into(prodImage);
		prodDesc = (TextView) findViewById(R.id.prodDesc);
		prodDesc.setText(product.getDescription());
		int countSize = product.getSize_price().size();
		seekBar = (DiscreteSeekBar) findViewById(R.id.discreteSeekBar);
		seekBar.setMin(0);
		seekBar.setMax(countSize - 1);
		seekBar.setOnProgressChangeListener(this);
		sizePrice = (TextView) findViewById(R.id.sizePrice);

		seekBar.setProgress(0);

		if (countSize == 1) {
			seekBar.setVisibility(View.INVISIBLE);
			sizePrice.setText(product.getSize_price().get(0).getPrice() + " руб");
		} else {
			sizePrice.setText(product.getSize_price().get(0).getSize() + " за " + product.getSize_price().get(0).getPrice() + " руб");
		}
	}


	@Override
	public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
		sizePrice.setText(product.getSize_price().get(value).getSize() + " за " + product.getSize_price().get(value).getPrice() + " руб");
	}

	@Override
	public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

	}

	@Override
	public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

	}
}
