package com.example.dima.amopizza.controller;

import android.content.Intent;
import android.view.View;
import com.example.dima.amopizza.activity.GridActivity;
import com.example.dima.amopizza.activity.StartActivity;
import com.example.dima.amopizza.model.GroupProduct;

import java.util.ArrayList;

public class MyMenuListener implements View.OnClickListener {
	ArrayList<GroupProduct> catalog;
	StartActivity parent;

	public MyMenuListener(StartActivity parent) {
		this.parent = parent;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId() - parent.getUserId();
		Intent intentGrid = new Intent(parent, GridActivity.class);
		catalog = parent.getCatalog();
		intentGrid.putParcelableArrayListExtra("catalog", catalog);
		intentGrid.putExtra("groupNumber", id);
		parent.startActivity(intentGrid);
	}
}
