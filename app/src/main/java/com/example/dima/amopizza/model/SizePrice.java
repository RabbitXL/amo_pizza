package com.example.dima.amopizza.model;

import android.os.Parcel;
import android.os.Parcelable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class SizePrice implements Parcelable {
	String size;
	String price;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.size);
		dest.writeString(this.price);
	}

	protected SizePrice(Parcel in) {
		this.size = in.readString();
		this.price = in.readString();
	}

	public static final Creator<SizePrice> CREATOR = new Creator<SizePrice>() {
		@Override
		public SizePrice createFromParcel(Parcel source) {
			return new SizePrice(source);
		}

		@Override
		public SizePrice[] newArray(int size) {
			return new SizePrice[size];
		}
	};

	@Override
	public String toString() {
		return "от " + price + " руб";
	}
}
